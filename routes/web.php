<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('homepage');

Route::resource("menu", "MenuController");
Route::resource("product", "ProductsController");
Route::resource('user', 'UserController');
Route::resource('admin', 'AdminController');