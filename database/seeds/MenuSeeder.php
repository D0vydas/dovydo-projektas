<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'title' => 'Grindų lentos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Gultų lentos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Lauko dailylentės',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Liepto lentos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Pakalimo lentos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Terasinės lentos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Tvoros lentos',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('menus')->insert([
            'title' => 'Vidaus dailylentės',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
