<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@user.lt',
            'password' => '$2y$10$ZvjXJxRWnAkCiyc7Ba6wKOswcw352Q5BZcdV/khzHqZGtQ2hgar2W',
            'remember_token' => 'Msxo6CB8LRaM0zkbwkPByKKRBCqdopkrt5KpkUkYHpZLLBwdSkvRtW5f4fIx',
            'is_admin' => '0',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.lt',
            'password' => '$2y$10$X4iJWIOEG2Y/w5X.PG69Me.mNCDe7fveNPbhnUjXq9G1cE8CZAdCa',
            'remember_token' => 'Ugc6jlggjgKux1HHjppBt5iGpyOmwKqnQ3dK6uVICh1WcCSPMyFIY5m2Kkaz',
            'is_admin' => '1',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
