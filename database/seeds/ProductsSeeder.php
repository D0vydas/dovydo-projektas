<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'title' => 'Grindų lentos',
            'photo' => 'grindu_lentos.png',
            'description' => '(Eglės, pušies) mediena<br>28x130 mm, 28x100 mm, 28x80 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '9.00 - 10.00 €',
            'menu_id' => '1',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Beržo gultai',
            'photo' => 'gultu_lentos1.png',
            'description' => '30x70 mm, 30x90 mm',
            'dimensions_price' => '1 metro kaina:',
            'price' => '1.50 - 2.00 €',
            'menu_id' => '2',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Drebulės gultai',
            'photo' => 'gultu_lentos2.png',
            'description' => '30x70 mm, 30x90 mm',
            'dimensions_price' => '1 metro kaina:',
            'price' => '1.50 - 2.00 €',
            'menu_id' => '2',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Juodalksnio gultai',
            'photo' => 'gultu_lentos3.png',
            'description' => '30x70 mm, 30x90 mm',
            'dimensions_price' => '1 metro kaina:',
            'price' => '1.50 - 2.00 €',
            'menu_id' => '2',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Liepos gultai',
            'photo' => 'gultu_lentos4.png',
            'description' => '30x70 mm, 30x90 mm',
            'dimensions_price' => '1 metro kaina:',
            'price' => '1.50 - 2.00 €',
            'menu_id' => '2',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Lauko dailylentės',
            'photo' => 'lauko_dailylentes1.png',
            'description' => '(Pušies, eglės) mediena<br>20x100 - 170 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 8.70 €',
            'menu_id' => '3',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Lauko dailylentės',
            'photo' => 'lauko_dailylentes2.png',
            'description' => '(Pušies, eglės) mediena<br>20x100 - 170 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 8.70 €',
            'menu_id' => '3',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Lauko dailylentės',
            'photo' => 'lauko_dailylentes3.png',
            'description' => '(Pušies, eglės) mediena<br>20x100 - 170 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 8.70 €',
            'menu_id' => '3',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Lauko dailylentės',
            'photo' => 'lauko_dailylentes4.png',
            'description' => '(Pušies, eglės) mediena<br>20x130 mm, 20x100 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 8.70 €',
            'menu_id' => '3',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Liepto lentos',
            'photo' => 'liepto_lentos1.png',
            'description' => '(Pušies, eglės) mediena<br>30x90 mm, 30x110 mm, 30x140 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '9.00 - 10.00 €',
            'menu_id' => '4',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Pakalimo lentos',
            'photo' => 'pakalimo_lentos1.png',
            'description' => '(Pušies, eglės) mediena<br>20x75 mm , 20x95 mm, 20x125 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.80 - 8.50 €',
            'menu_id' => '5',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Pakalimo lentos',
            'photo' => 'pakalimo_lentos2.png',
            'description' => '(Pušies, eglės) mediena<br>20x90 mm, 20x110 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '5.20 - 7.20 €',
            'menu_id' => '5',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Terasinės lentos',
            'photo' => 'terasines_lentos1.png',
            'description' => '(Pušies, eglės) mediena<br>30x140 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '9.00 - 10.00 €',
            'menu_id' => '6',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Terasinės lentos',
            'photo' => 'terasines_lentos2.png',
            'description' => '(Pušies, eglės) mediena<br>30x140 mm, 30x110 mm, 30x90 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '9.00 - 10.00 €',
            'menu_id' => '6',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Tvoros lentos',
            'photo' => 'tvoros_lentos1.png',
            'description' => '(Pušies, eglės) mediena<br>20x90 mm, 20x110 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '5.20 - 7.20 €',
            'menu_id' => '7',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('products')->insert([
            'title' => 'Drebulės dailylentė',
            'photo' => 'vidaus_dailylentes1.png',
            'description' => '15x60 mm, 15x80 mm, 15x100 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 12.00 €',
            'menu_id' => '8',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Juodalksnio dailylentė',
            'photo' => 'vidaus_dailylentes2.png',
            'description' => '15x60 mm, 15x80 mm, 15x100 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 12.00 €',
            'menu_id' => '8',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Liepos dailylentė',
            'photo' => 'vidaus_dailylentes3.png',
            'description' => '15x60 mm, 15x80 mm, 15x100 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '7.00 - 12.00 €',
            'menu_id' => '8',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        DB::table('products')->insert([
            'title' => 'Pušies, eglės dailylentė',
            'photo' => 'vidaus_dailylentes4.png',
            'description' => '15x60 mm, 15x80 mm, 15x100 mm',
            'dimensions_price' => '1 kvadrato kaina:',
            'price' => '5.20 - 5.80 €',
            'menu_id' => '8',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
