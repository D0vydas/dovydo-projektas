@extends("layouts.main")

@section("content")

<section id="interior">
  <div class="container-internal">
    <div class="panel panel-default">
      <div class="panel-body">
        <a href="{{ URL::route('product.create') }}" class="btn btn-primary btn-xs pull-left"><b>+</b> Pridėti naują gaminį</a>
        <table class="table table-striped custab">
        @if(count($products) == 0)
                   <div class="text-center col-md-12">
                     <i class="fa fa-warning"></i>
                     <p>Gaminių nėra.</p>
                   </div>
        @else
          <thead>
            <tr>
              <th></th>
              <th>Pavadinimas</th>
              <th>Kaina</th>
              <th>Aprašymas</th>
              <th class="text-center">Veiksmas</th>
            </tr>
          </thead>
          @foreach($products as $menu)
          <tr>
            <td><img src="{{ asset('img/products_photo') }}/{{ $menu->photo }}" alt="{{ $menu->photo }}" height="30"></td>
            <td>{{ $menu->title }}</td>
            <td>{{ $menu->dimensions_price }}<br>{{ $menu->price }} </td>
            <td>{!! $menu->description !!}</td>
            <td class="text-center">
              <div class="col-xs-6 text-right">
              <a href="{{ route('product.edit', $menu->id) }}" class='btn btn-info btn-xs'>
                  <span class="glyphicon glyphicon-edit"></span> 
                  Redaguoti
              </a> 
              </div>
              <div class="col-xs-6 text-left">
              {{ Form::open(['route' => ['product.destroy', $menu->id], 'method' => "POST"]) }}
              {{ Form::hidden('_method', 'DELETE') }}
                      {{ csrf_field() }}
                  {{Form::button('<span class="glyphicon glyphicon-remove"></span> Trinti', array('type' => 'submit', 'class' => 'btn btn-danger btn-xs'))}}
              {{ Form::close() }}
              </div>
            </td>
          </tr>
          @endforeach
        @endif
        </table>
      </div>
    </div>
  </div>
</section>

@endsection