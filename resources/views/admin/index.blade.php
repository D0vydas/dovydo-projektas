@extends("layouts.main")

@section("content")

<section id="interior">
  <div class="container-internal">
    <div class="panel panel-default">
      <div class="panel-body">
        <a href="{{ route('admin.create') }}" class="btn btn-primary btn-xs pull-left"><b>+</b> Pridėti naują meniu</a>
        <table class="table table-striped custab">
          <thead>
            <tr>
              <th>ID</th>
              <th>Pavadinimas</th>
              <th>Redaguota</th>
              <th class="text-center">Veiksmas</th>
            </tr>
          </thead>
          @foreach($menus as $menu)
          <tr>
            <td>{{ $menu->id }}</td>
            <td><a href="{{ route('admin.show', $menu->id) }}">{{ $menu->title }} <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></td>
            <td>{{ $menu->updated_at }}</td>
            <td class="text-center">
              <div class="col-xs-6 text-right">
              <a href="{{ route('admin.edit', $menu->id) }}" class='btn btn-info btn-xs'>
                  <span class="glyphicon glyphicon-edit"></span> 
                  Redaguoti
              </a> 
              </div>
              <div class="col-xs-6 text-left">
              {{ Form::open(['route' => ['admin.destroy', $menu->id], 'method' => "POST"]) }}
              {{ Form::hidden('_method', 'DELETE') }}
                      {{ csrf_field() }}
                  {{Form::button('<span class="glyphicon glyphicon-remove"></span> Trinti', array('type' => 'submit', 'class' => 'btn btn-danger btn-xs'))}}
              {{ Form::close() }}
              </div>
            </td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</section>

@endsection