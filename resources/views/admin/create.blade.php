@extends("layouts.main")

@section("content")


<section id="interior">
    <div class="container-internal">
        <div class="panel panel-default">
            <legend>{{ isset($menu) ? 'Redaguoti meniu' : 'Pridėti naują meniu' }}</legend>
            <div class="panel-body">

				@if(session('messages'))
					<div class="box-body">
				        <div class="alert alert-warning alert-dismissible">
				        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				       		<h4><i class="icon fa fa-warning"></i> Yra klaidų!</h4>
				       		<ul>
				       		@foreach(session('messages')->all() as $error)
				        		<li>{{ $error }}</li>
				        	@endforeach
				        	</ul>
				    	</div>
				    </div>
			    @endif

				@if(isset($menu))
					{{ Form::open(['route' => ['admin.update', $menu->id], 'method' => "POST"]) }}
					{{ Form::hidden('_method', 'PUT') }}
				@else
					{{ Form::open(['route' => 'admin.store', 'method' => "POST"]) }}
				@endif 

				{{ csrf_field() }}

					{{ Form::label('title', 'Pavadinimas') }}
					@if(isset($menu))
						{{ Form::text('title', ($menu['title']), ['class' => 'form-control']) }}
					@else
						{{ Form::text('title', '', ['class' => 'form-control']) }}
					@endif


				@if(isset($menu))
					{{ Form::submit('Redaguoti', ['class' => 'btn btn-primary btn-xs']) }}
				@else
					{{ Form::submit('+ Pridėti', ['class' => 'btn btn-primary btn-xs']) }}
				@endif


				{{ Form::close() }}

				@if(isset($menu))
					{{ Form::open(['route' => ['admin.destroy', $menu->id], 'method' => "POST"]) }}
					{{ Form::hidden('_method', 'DELETE') }}
							{{ csrf_field() }}
						{{ Form::submit('Trinti', ['class' => 'btn btn-danger btn-xs']) }}
					{{ Form::close() }}
				@endif

            </div>
        </div>
    </div>
</section>

@endsection