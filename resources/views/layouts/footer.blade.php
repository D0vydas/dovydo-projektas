		<footer>
			<div id="footer-menu">
				<div class="container-internal row">
					<div class="col-md-4 col-sm-6">
						<h4>Siūlome</h4>
						<ul>
							<li><a href="#" alt="aaa">Statybinė mediena</a></li>
							<li><a href="#" alt="">Granulės</a></li>
						</ul>
						<h4>Informacija</h4>
						<ul>
							<li><a href="#" alt="aaa">Apie mūsų įmonę</a></li>
							<li><a href="#" alt="">Mes perkame</a></li>
							<li><a href="#" alt="">Karjera</a></li>
							<li><a href="#" alt="">Kontaktai</a></li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-6">
						<h4>Gaminiai</h4>
						<ul>
							@foreach (\App\Menu::all() as $menu)
			                  <li><a href="{{ route('product.show', $menu->id) }}">{{ $menu->title }}</a></li>
			                @endforeach
						</ul>
					</div>
					<div class="col-md-4">
						<h4>Kontaktai</h4>
						<ul>
							<li><a href="#" alt=""></a><b>+370 677 14 880</b></li>
							<li><small>Aidas</small></li>
							<li><a href="#" alt=""></a><b>+370 645 25 921</b></li>
							<li><small>Evaldas</small></li>
							<li class="divider"></li>
							<li><a href="mailto:info@sirvintumediena.lt"><small>info@sirvintumediena.lt</small></a></li>
							<li><a href="mailto:sirvintumediena@gmail.com"><small>sirvintumediena@gmail.com</small></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div id="footer">
				<div class="container-internal row">
					<div class="pull-left">
						<p>UAB Širvintų mediena © 2013-2016</p>
					</div>
					<div class="pull-right">
						<p>WEB Sprendimas: <a href="mailto:dovydas@upsas.lt">Dovydas</a></p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<!-- js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="{{ asset('js/my_js.js') }}"></script>
</body>
</html>