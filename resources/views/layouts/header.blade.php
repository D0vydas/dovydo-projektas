<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Širvintų mediena</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500&amp;subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600" rel="stylesheet">
  <!-- CSS and font-awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/mano-styles.css') }}">
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
</head>
<body>
<div class="container">
<header>
  <nav class="navbar navbar-default">
    <div class="container_nav">
      <div class="navbar-header">
        <a  class="menuBtn" data-toggle="collapse" data-target="#nav" aria-expanded="false" aria-controls="navbar">
        <span class="lines"></span>
        </a>
      </div>
      <a class="navbar-brand" href="{{ URL::route('homepage') }}"><img src="{{ asset('img/logo.png') }}" alt="Logotipas"/></a>
      <div id="nav" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left visible-xs visible-sm text-center">
          <li class="dropdown-header">Siūlome</li>
          <li>
              <a href="#">Statybinę medieną</a>
          </li>
          <li>
              <a href="#">Granules</a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gaminius <span class="caret"></span></a>
            <ul class="dropdown-menu">
              @foreach (\App\Menu::all() as $menu)
                <li><a href="{{ route('product.show', $menu->id) }}">{{ $menu->title }}</a></li>
              @endforeach
            </ul>
          </li>
          <li class="dropdown-header">Informacija</li>
          <li>
              <a href="#">Apie mūsų įmonę</a>
          </li>
          <li>
              <a href="#">Kontaktai</a>
          </li>
          <li>
              <a href="#">Karjera</a>
          </li>
          <li>
              <a href="#">Mes perkame</a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-left visible-md visible-lg">
        <li class="dropdown">
        <a class="menuBtn" data-target="dropdown-menu" data-toggle="dropdown">
          <span class="lines"></span>
        </a>
          <div class="dropdown-menu mega-dropdown-menu row">
            <div class="arrow"></div>
              <div class="col-sm-6">
                <ul class="columns-menu">
                  <li class="dropdown-header">Siūlome</li>
                  <li><a href="#">Statybinę medieną</a></li>
                  <li><a href="#">Granules</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Informacija</li>
                  <li><a href="#" alt="aaa">Apie mūsų įmonę</a></li>
                  <li><a href="#" alt="">Mes perkame</a></li>
                  <li><a href="#" alt="">Karjera</a></li>
                  <li><a href="#" alt="">Kontaktai</a></li>
                </ul>
              </div>
              <div class="col-sm-6">
                <ul class="columns-menu">
                  <li class="dropdown-header">Gaminiai</li>
                  @foreach (\App\Menu::all() as $menu)
                    <li><a href="{{ route('product.show', $menu->id) }}">{{ $menu->title }}</a></li>
                  @endforeach
                </ul>
              </div>
          </div>
        </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="visible-md visible-lg">
          @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Prisijungti</a></li>
          @else
            <li>
              <a href="{{ route('user.show', Auth::user()->id) }}">
                {{ Auth::user()->name }} <img src="{{ asset('img/user.png') }}" width="20px" alt=""/>
              </a>
            </li>
          @endif
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>