@extends("layouts.main")

@section("content")

  @if($request->is('/'))
    <div class="background-video">
      <video poster="{{ asset('img/header_bg.png') }}" id="bgvid" playsinline autoplay muted loop>
        <source src="{{ asset('video/home.webm') }}" type="video/webm">
      </video>
      <div class="background"></div>
      <div class="background-video-text">
        <p>Širvintų mediena</p>
      </div>
    </div>
  @endif

<section id="three-columns">
  <div class="container-internal">
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <div class="three-well">
          <div class="three-reviews-img">
            <img class="three-img-thumbnail" src="http://sirvintumediena.lt/images/slider/2015_1.png" />
          </div>
          <div class="three-media-body">
            <h4 class="three-media-heading">Statybinė mediena</h4>
            <p class="divider"></p>
            <p>Dvigubo pjovimo <span class="color-blue">160.00 €</span> m³<br>
            Viengubo pjovimo <span class="color-blue">58.00 - 73.00 €</span> m³</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="three-well">
        <div class="ribbon"><span>Rekomenduojame</span></div>
          <div class="three-reviews-img">
            <img class="three-img-thumbnail" src="http://sirvintumediena.lt/images/slider/2015_2.png" />
          </div>
          <div class="three-media-body">
            <h4 class="three-media-heading"><span class="color-green">EKO</span>logiškos medžio granulės</h4>
            <p class="divider"></p>
            <p>Kaina: <span class="color-blue">174.00 €</span> už TONĄ</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="three-well">
          <div class="three-reviews-img">
            <img class="three-img-thumbnail" src="http://sirvintumediena.lt/images/slider/2015_3.png" />
          </div>
          <div class="three-media-body">
            <h4 class="three-media-heading">Perkame rąstus ir statų mišką</h4>
            <p class="divider"></p>
            <p>Pasiteiravimui:<br>
            Tel. <span class="color-blue">+370 677 14 880</span><br>
            Tel. <span class="color-blue">+370 645 25 921</span></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="about-service">
  <div class="container-internal">
    <div class="center title">Šiek tiek apie mus…</div>
    <p><b>UAB Širvintų mediena</b> – įmonė, pasižyminti gausiu medienos asortimentu. Per trumpą laiką spėjome išsiugdyti didelį žmonių pasitikėjimą.
    Siūlome Jums aukštos kokybės medienos gaminius, greitą aptarnavimą bei lanksčias ir patrauklias bendradarbiavimo sąlygas.
    Savo klientams siūlome tik aukštos kokybės medieną, kurios kainos yra prieinamos kiekvienam klientui.</p>
    <div class="row saw">
      <div class="col-md-6">
        <h2>Siūlome jums:</h2>
        <ul>
          <li>Statybinę medieną už prieinamą kainą</li>
          <li>EKOlogiškas medžio granules</li>
          <li>Grindų lentas</li>
          <li>Gultų lentas</li>
          <li>Lauko dailylentes</li>
          <li>Liepto lentas</li>
          <li>Pakalimo lentas</li>
          <li>Terasines lentas</li>
          <li>Tvoros lentas</li>
          <li>Vidaus dailylentes</li>
        </ul>
      </div>
      <div class="col-md-6 col-sm-5 col-xs-4 saw-img">
        <img class="saw-img img-responsive" src="img/saw.png">
      </div>
    </div>
  </div>
</section>
<section id="info">
  <div class="container-internal">
    <div class="row">
      <div class="col-md-4 col-sm-6">
        <img class="img-responsive" src="img/medal_icon.png">
        <span>98%</span>
        <p>Patenkintų klientų</p>
      </div>
      <div class="col-md-4 col-sm-6">
        <img class="img-responsive" src="img/win_icon.png">
        <span>9.5/10</span>
        <p>Jūsų įvertinimas</p>
      </div>
      <div class="col-md-4">
        <img class="img-responsive" src="img/coffee_icon.png">
        <span>3.245</span>
        <p>Išgerti puodeliai kavos</p>
      </div>
    </div>
  </div>
</section>
<section id="friends">
  <div class="container-internal">
  <div class="center title">Mūsų draugai</div>
    <div id="myCarouselWrapper" class="container-fluid">
      <div id="myCarousel" class="carousel slide">
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="item-item col-md-3 col-sm-4"><a href="1"><img class="img-responsive" src="img/logo_1.png"></a></div>
            </div>
            <div class="item">
              <div class="item-item col-md-3 col-sm-4"><a href="2"><img class="img-responsive" src="img/logo_2.png"></a></div>
            </div>
            <div class="item">
              <div class="item-item col-md-3 col-sm-4"><a href="3"><img class="img-responsive" src="img/logo_3.png"></a></div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Praeitas</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Kitas</span>
        </a>
      </div>
    </div>
  </div>
</section>
<section id="reviews">
  <div class="container-internal">
  <div class="center title">Klientų atsiliepimai</div>
    <div class="bg-info-reviews">
      <div id="carousel-example-vertical" class="carousel vertical slide">
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <p class="ticker-headline">
              <div class="well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="reviews-img">
                  <img class="img-thumbnail" src="http://lorempixel.com/140/140/" />
                </div>
                <span class="pull-right">2016-11-22</span>
                <div class="media-body">
                  <h4 class="media-heading">John Doe1 <span>UAB Bosas</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras luctus eu odio fermentum tempus. Aliquam erat volutpat.</p>
                </div>
              </div>
            </p>
          </div>
          <div class="item">
            <p class="ticker-headline">
              <div class="well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="reviews-img">
                  <img class="img-thumbnail" src="http://lorempixel.com/140/140/" />
                </div>
                <span class="pull-right">2016-11-22</span>
                <div class="media-body">
                  <h4 class="media-heading">John Doe2 <span>UAB Bosas</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras luctus eu odio fermentum tempus. Aliquam erat volutpat.</p>
                </div>
              </div>
            </p>
          </div>
          <div class="item">
            <p class="ticker-headline">
              <div class="well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="reviews-img">
                  <img class="img-thumbnail" src="http://lorempixel.com/140/140/" />
                </div>
                <span class="pull-right">2016-11-22</span>
                <div class="media-body">
                  <h4 class="media-heading">John Doe3 <span>UAB Bosas</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras luctus eu odio fermentum tempus. Aliquam erat volutpat.</p>
                </div>
              </div>
            </p>
          </div>
          <div class="item">
            <p class="ticker-headline">
              <div class="well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="reviews-img">
                  <img class="img-thumbnail" src="http://lorempixel.com/140/140/" />
                </div>
                <span class="pull-right">2016-11-22</span>
                <div class="media-body">
                  <h4 class="media-heading">John Doe4 <span>UAB Bosas</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras luctus eu odio fermentum tempus. Aliquam erat volutpat.</p>
                </div>
              </div>
            </p>
          </div>
          <div class="item">
            <p class="ticker-headline">
              <div class="well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="reviews-img">
                  <img class="img-thumbnail" src="http://lorempixel.com/140/140/" />
                </div>
                <span class="pull-right">2016-11-22</span>
                <div class="media-body">
                  <h4 class="media-heading">John Doe5 <span>UAB Bosas</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras luctus eu odio fermentum tempus. Aliquam erat volutpat.</p>
                </div>
              </div>
            </p>
          </div>
        </div>
        <a class="up carousel-control" href="#carousel-example-vertical" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="down carousel-control" href="#carousel-example-vertical" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div> 
</section>

@endsection