@extends("layouts.main")

@section("content")

  @if($request->is('product/*'))
    <div class="background-cover cover-{{ $menu->id }}">
      <div class="background-color"></div>
      <div class="background-cover-text">
        <p>{{ $menu->title }}</p>
      </div>
    </div>
  @endif

<section>
	<div id="products">
		<div class="container-internal">
			<div class="row">
			@foreach ($products->reverse() as $product)
				<div class="products-well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
					<div class="products-reviews-img">
						<img class="products-img-thumbnail" src="{{ asset('') }}img/products_photo/{{$product->photo}}" />
					</div>
					<span class="pull-right">{{ $product->price }}</span>
					<div class="products-media-body">
						<h4 class="products-media-heading">{{ $product->title }}</h4>
						<p>{!! $product->description !!}</p>
						<p class="divider"></p>
						<p>{{ $product->dimensions_price }} {{ $product->price }}</p>
					</div>
				</div>
			@endforeach
			</div>
		</div>
	</div>
</section>

@endsection