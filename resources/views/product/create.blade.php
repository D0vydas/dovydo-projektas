@extends("layouts.main")

@section("content")


<section id="interior">
    <div class="container-internal">
        <div class="panel panel-default">
            <legend>{{ isset($product) ? 'Redaguoti gaminį' : 'Pridėti naują gaminį' }}</legend>
            <div class="panel-body">

				@if(session('messages'))
					<div class="box-body">
				        <div class="alert alert-warning alert-dismissible">
				        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				       		<h4><i class="icon fa fa-warning"></i> Yra klaidų!</h4>
				       		<ul>
				       		@foreach(session('messages')->all() as $error)
				        		<li>{{ $error }}</li>
				        	@endforeach
				        	</ul>
				    	</div>
				    </div>
			    @endif

				@if(isset($product))
					{{ Form::open(['route' => ['product.update', $product->id], 'method' => "POST"]) }}
					{{ Form::hidden('_method', 'PUT') }}
				@else
					{{ Form::open(['route' => 'product.store', 'method' => "POST"]) }}
				@endif 

				{{ csrf_field() }}

					<div class="row">
                        <div class="col-md-6">
                            {{ Form::label('title', 'Pavadinimas') }}
							@if(isset($product))
								{{ Form::text('title', ($product['title']), ['class' => 'form-control']) }}
							@else
								{{ Form::text('title', '', ['class' => 'form-control']) }}
							@endif
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('photo', 'Nuotraukos pavadinimas') }}
							@if(isset($product))
								{{ Form::text('photo', ($product['photo']), ['class' => 'form-control']) }}
							@else
								{{ Form::text('photo', 'PVZ: nuotrauka.jpg (būtina įkelti į folderį) (greit veiks auto įkėlimas)', ['class' => 'form-control']) }}
							@endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            {{ Form::label('dimensions_price', 'Dydžio kainą') }}
							@if(isset($product))
								{{ Form::text('dimensions_price', ($product['dimensions_price']), ['class' => 'form-control']) }}
							@else
								{{ Form::text('dimensions_price', 'PVZ: 1 kvadrato kaina:', ['class' => 'form-control']) }}
							@endif
                        </div>
                        <div class="col-md-4">
                            {{ Form::label('price', 'Kaina') }}
							@if(isset($product))
								{{ Form::text('price', ($product['price']), ['class' => 'form-control']) }}
							@else
								{{ Form::text('price', '€', ['class' => 'form-control']) }}
							@endif
                        </div>
                        <div class="col-md-4">
							{{ Form::label('menu_id', 'Kategorija') }}
							<select class="form-control" name="menu_id">
							    @foreach (\App\Menu::all() as $menu)
							    	<option value="{{ $menu->id }}">{{ $menu->title }}</option>
							    @endforeach
							</select>
                        </div>
                    </div>

				{{ Form::label('description', 'Aprašymas') }}
				@if(isset($product))
					{{ Form::textarea('description', ($product['description'])) }}
				@else
					{{ Form::textarea('description') }}
				@endif

				@if(isset($product))
					{{ Form::submit('Redaguoti', ['class' => 'btn btn-primary btn-xs']) }}
				@else
					{{ Form::submit('+ Pridėti', ['class' => 'btn btn-primary btn-xs']) }}
				@endif


				{{ Form::close() }}

				@if(isset($product))
					{{ Form::open(['route' => ['admin.destroy', $product->id], 'method' => "POST"]) }}
					{{ Form::hidden('_method', 'DELETE') }}
							{{ csrf_field() }}
						{{ Form::submit('Trinti', ['class' => 'btn btn-danger btn-xs']) }}
					{{ Form::close() }}
				@endif

            </div>
        </div>
    </div>
</section>

@endsection