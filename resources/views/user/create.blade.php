@extends("main")

@section("content")

<h3>{{ isset($user) ? 'Edit user' : 'Add user' }}</h3>


@if(isset($errorMessages))
    <ul class="error">
        <h4>Yra klaidu</h4>
        @foreach($errorMessages->all() as $error)
            <li>
                {{ $error }}
            </li>
        @endforeach
    </ul>
@endif



@if(isset($user))
	{{ Form::open(['route' => ['user.update', $user->id], 'method' => "POST"]) }}
	{{ Form::hidden('_method', 'PUT') }}
@else
	{{ Form::open(['route' => 'user.store', 'method' => "POST"]) }}
@endif 

{{ csrf_field() }}

	{{ Form::label('name', 'Name') }}
	@if(isset($user))
		{{ Form::text('name', ($user['name']), ['class' => 'form-control']) }}
	@else
		{{ Form::text('name', '', ['class' => 'form-control']) }}
	@endif

	{{ Form::label('email', 'E-mail') }}
	@if(isset($user))
		{{ Form::text('email', ($user['email']), ['class' => 'form-control']) }}
	@else
		{{ Form::text('email', '', ['class' => 'form-control']) }}
	@endif

	@if(isset($user))
		{{ Form::label('is_admin', 'Admin?') }}
		{{ Form::hidden('is_admin', 0 ) }}
		{{ Form::checkbox('is_admin', 1, $user->is_admin) }}
	@else
	@endif

	@if(isset($user))
	@else
		{{ Form::label('password', 'Password') }}
		{{ Form::text('password', '', ['class' => 'form-control']) }}
	@endif


@if(isset($user))
	{{ Form::submit('Edit user', ['class' => 'btn btn-primary']) }}
@else
	{{ Form::submit('Add user', ['class' => 'btn btn-primary']) }}
@endif


{{ Form::close() }}

@if(isset($user))
	{{ Form::open(['route' => ['user.destroy', $user->id], 'method' => "POST"]) }}
	{{ Form::hidden('_method', 'DELETE') }}
			{{ csrf_field() }}
		{{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
	{{ Form::close() }}
@endif



@endsection