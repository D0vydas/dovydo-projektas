@extends("layouts.main")

@section("content")

<section id="interior">
    <div class="container-internal">
        <div class="margin-bottom">
            <a class="pull-left" href="#">
                <img class="media-object dp img-circle" src="{{ asset('img/user.png') }}" style="width: 100px;height:100px;">
            </a>
            <div class="media-body">
                <div class="media-heading">
                    <small>
                        @php
                            switch ($user->is_admin) {
                                case 3:
                                    echo 'Darbuotojas';
                                    break;
                                case 2:
                                    echo 'Administracija';
                                    break;
                                case 1:
                                    echo 'Vadovas';
                                    break;
                                default:
                                    echo 'Lankytojas';
                            }
                        @endphp
                    </small>
                    {{ $user->name }}
                    <div class="pull-right">
                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-xs btn-default btn-flat">
                      Atsijungti
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                    </div>
                </div>
                <hr style="margin:8px auto">
                <span>Redaguoti:</span><br>
                <a href="{{ route('user.create') }}" class="btn btn-default btn-xs disabled">Savo profilį</a>
                <a href="{{ route('admin.index') }}" class="btn btn-primary btn-xs">meniu ir gaminius</a>
            </div>
        </div>
        <div class="panel panel-default">
            <legend>Įvertink mūsų paslaugas</legend>
            <div class="panel-body">
                Neužilgo
            </div>
        </div>
        <div class="panel panel-default">
            <legend>Palik atsiliepimą</legend>
            <div class="panel-body">
                Neužilgo
            </div>
        </div>
    </div>
</section>

@endsection