-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016 m. Grd 12 d. 15:00
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirvintumediena`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `menus`
--

INSERT INTO `menus` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Grindų lentos', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(2, 'Gultų lentos', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(3, 'Lauko dailylentės', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(4, 'Liepto lentos', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(5, 'Pakalimo lentos', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(6, 'Terasinės lentos', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(7, 'Tvoros lentos', '2016-12-06 20:41:40', '2016-12-06 20:41:40'),
(8, 'Vidaus dailylentės', '2016-12-06 20:41:40', '2016-12-06 20:41:40');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2016_12_04_201212_menus_table', 1),
(9, '2016_12_04_205747_products_table', 1),
(10, '2016_12_06_012048_add_isadmin_to_users_table', 1);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `dimensions_price` text COLLATE utf8_unicode_ci,
  `price` text COLLATE utf8_unicode_ci,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `products`
--

INSERT INTO `products` (`id`, `title`, `photo`, `description`, `dimensions_price`, `price`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 'Grindų lentos', 'grindu_lentos.png', '(Eglės, pušies) mediena<br>28x130 mm, 28x100 mm, 28x80 mm', '1 kvadrato kaina:', '9.00 - 10.00 €', 1, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(2, 'Beržo gultai', 'gultu_lentos1.png', '30x70 mm, 30x90 mm', '1 metro kaina:', '1.50 - 2.00 €', 2, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(3, 'Drebulės gultai', 'gultu_lentos2.png', '30x70 mm, 30x90 mm', '1 metro kaina:', '1.50 - 2.00 €', 2, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(4, 'Juodalksnio gultai', 'gultu_lentos3.png', '30x70 mm, 30x90 mm', '1 metro kaina:', '1.50 - 2.00 €', 2, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(5, 'Liepos gultai', 'gultu_lentos4.png', '30x70 mm, 30x90 mm', '1 metro kaina:', '1.50 - 2.00 €', 2, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(6, 'Lauko dailylentės', 'lauko_dailylentes1.png', '(Pušies, eglės) mediena<br>20x100 - 170 mm', '1 kvadrato kaina:', '7.00 - 8.70 €', 3, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(7, 'Lauko dailylentės', 'lauko_dailylentes2.png', '(Pušies, eglės) mediena<br>20x100 - 170 mm', '1 kvadrato kaina:', '7.00 - 8.70 €', 3, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(8, 'Lauko dailylentės', 'lauko_dailylentes3.png', '(Pušies, eglės) mediena<br>20x100 - 170 mm', '1 kvadrato kaina:', '7.00 - 8.70 €', 3, '2016-12-06 20:41:41', '2016-12-06 20:41:41'),
(9, 'Lauko dailylentės', 'lauko_dailylentes4.png', '(Pušies, eglės) mediena<br>20x130 mm, 20x100 mm', '1 kvadrato kaina:', '7.00 - 8.70 €', 3, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(10, 'Liepto lentos', 'liepto_lentos1.png', '(Pušies, eglės) mediena<br>30x90 mm, 30x110 mm, 30x140 mm', '1 kvadrato kaina:', '9.00 - 10.00 €', 4, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(11, 'Pakalimo lentos', 'pakalimo_lentos1.png', '(Pušies, eglės) mediena<br>20x75 mm , 20x95 mm, 20x125 mm', '1 kvadrato kaina:', '7.80 - 8.50 €', 5, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(12, 'Pakalimo lentos', 'pakalimo_lentos2.png', '(Pušies, eglės) mediena<br>20x90 mm, 20x110 mm', '1 kvadrato kaina:', '5.20 - 7.20 €', 5, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(13, 'Terasinės lentos', 'terasines_lentos1.png', '(Pušies, eglės) mediena<br>30x140 mm', '1 kvadrato kaina:', '9.00 - 10.00 €', 6, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(14, 'Terasinės lentos', 'terasines_lentos2.png', '(Pušies, eglės) mediena<br>30x140 mm, 30x110 mm, 30x90 mm', '1 kvadrato kaina:', '9.00 - 10.00 €', 6, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(15, 'Tvoros lentos', 'tvoros_lentos1.png', '(Pušies, eglės) mediena<br>20x90 mm, 20x110 mm', '1 kvadrato kaina:', '5.20 - 7.20 €', 7, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(16, 'Drebulės dailylentė', 'vidaus_dailylentes1.png', '15x60 mm, 15x80 mm, 15x100 mm', '1 kvadrato kaina:', '7.00 - 12.00 €', 8, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(17, 'Juodalksnio dailylentė', 'vidaus_dailylentes2.png', '15x60 mm, 15x80 mm, 15x100 mm', '1 kvadrato kaina:', '7.00 - 12.00 €', 8, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(18, 'Liepos dailylentė', 'vidaus_dailylentes3.png', '15x60 mm, 15x80 mm, 15x100 mm', '1 kvadrato kaina:', '7.00 - 12.00 €', 8, '2016-12-06 20:41:42', '2016-12-06 20:41:42'),
(19, 'Pušies, eglės dailylentė', 'vidaus_dailylentes4.png', '15x60 mm, 15x80 mm, 15x100 mm', '1 kvadrato kaina:', '5.20 - 5.80 €', 8, '2016-12-06 20:41:42', '2016-12-06 20:41:42');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_admin`) VALUES
(1, 'User', 'user@user.lt', '$2y$10$ZvjXJxRWnAkCiyc7Ba6wKOswcw352Q5BZcdV/khzHqZGtQ2hgar2W', 'FgFcEbZOfK7W1GOOawsL1QYQNfIJ2BNOE6izf1VK3LxOEb8x1tvhKMBmhdXI', '2016-12-06 20:41:42', '2016-12-06 20:42:39', 0),
(2, 'Admin', 'admin@admin.lt', '$2y$10$X4iJWIOEG2Y/w5X.PG69Me.mNCDe7fveNPbhnUjXq9G1cE8CZAdCa', 'Ugc6jlggjgKux1HHjppBt5iGpyOmwKqnQ3dK6uVICh1WcCSPMyFIY5m2Kkaz', '2016-12-06 20:41:42', '2016-12-06 20:41:42', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
