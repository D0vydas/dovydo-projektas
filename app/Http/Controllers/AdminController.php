<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Menu;
use Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(\Auth::guest()
                || (!\Auth::guest() && \Auth::user()->is_admin == 0))
            {
                return redirect()->route('homepage');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();
        return view('admin.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required'
        );

        $messages = array(
            'required' => 'Laukelis :attribute turi būti užpildytas'
        );
        
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->passes()) {
            $success = \App\Menu::create($request->all());
            return redirect()->route('admin.index');
        } else {
            $errorMessages = $validator->messages();
            return redirect()->route("admin.create")->with("messages", $errorMessages);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = \App\Menu::find($id);
        $products = \App\Product::where("menu_id", $id)->get();

        return view("admin.show", compact("products", "menu"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = \App\Menu::find($id);
        return view('admin.create', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'title' => 'required'
        );

        $messages = array(
            'required' => 'Laukelis :attribute turi būti užpildytas'
        );
        
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->passes()) {
            $success = \App\Menu::find($id)->update($request->all());
            return redirect()->route('admin.index');
        } else {
            $errorMessages = $validator->messages();
            return redirect()->route("admin.edit", $id)->with("messages", $errorMessages);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Menu::find($id)->delete();
        return redirect()->route('admin.index');
    }
}
