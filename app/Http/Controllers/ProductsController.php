<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Validator;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $product = \App\Product::all();
        // return view('product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'photo' => 'required',
            'dimensions_price' => 'required',
            'price' => 'required',
            'description' => 'required'
        );

        $messages = array(
            'required' => 'Laukelis :attribute turi būti užpildytas'
        );
        
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->passes()) {
            $success = \App\Product::create($request->all());
            return redirect()->route('admin.index');
        } else {
            $errorMessages = $validator->messages();
            return redirect()->route("product.create")->with("messages", $errorMessages);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $menu = \App\Menu::find($id);
        $products = \App\Product::where("menu_id", $id)->get();

        return view("product.show", compact("products", "menu", "request"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Product::find($id);
        return view('product.create', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'title' => 'required',
            'photo' => 'required',
            'dimensions_price' => 'required',
            'price' => 'required',
            'description' => 'required'
        );

        $messages = array(
            'required' => 'Laukelis :attribute turi būti užpildytas'
        );
        
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->passes()) {
            $success = \App\Product::find($id)->update($request->all());
            return redirect()->route('admin.index');
        } else {
            $errorMessages = $validator->messages();
            return redirect()->route("product.edit", $id)->with("messages", $errorMessages);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Product::find($id)->delete();
        return redirect()->route('admin.index');
    }
}
