<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'title',
    	'photo',
    	'description',
    	'dimensions_price',
    	'price',
    	'menu_id'
    ];
    
    public function menu()
    {
    	return $this->belongsTo('App\Menu');
    }
}
